package com.labs.labsapplication

import android.app.Application
import com.labs.labsapplication.repo.GameRepository

/**
 * @author Ruslan Hlushan on 12/10/18.
 */
class LabsApp : Application() {

    override fun onCreate() {
        super.onCreate()
        GameRepository.init(this)
    }
}