package com.labs.labsapplication.main

import android.support.annotation.StringRes
import com.labs.labsapplication.R

/**
 * @author Ruslan Hlushan on 12/10/18.
 */
data class MenuItem(val id: Long, val name: String, val menuType: MenuType)

enum class MenuType(@StringRes val nameResId: Int) {
    NEW_GAME(R.string.new_game),
    CONTINUE(R.string.continue_game);
}

sealed class MenuItemSortType(val comparator: Comparator<MenuItem>) {
    class ID : MenuItemSortType(Comparator { o1, o2 -> o1.id.compareTo(o2.id) })
    class NAME : MenuItemSortType(Comparator { o1, o2 -> o1.name.compareTo(o2.name) })
}