package com.labs.labsapplication.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.labs.labsapplication.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.menu_item.*

/**
 * @author Ruslan Hlushan on 12/10/18.
 */
class MenuRecyclerAdapter(items: List<MenuItem>, private val itemClickListener: (MenuType) -> Unit) :
    RecyclerView.Adapter<MenuRecyclerAdapter.ViewHolder>() {

    private val items: MutableList<MenuItem> = items.toMutableList()

    init {
        setHasStableIds(true)
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.menu_item, parent, false), itemClickListener)

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) = viewHolder.bind(items.getOrNull(position))

    override fun getItemId(position: Int): Long = items.getOrNull(position)?.id ?: position.toLong()

    fun sortBy(sortType: MenuItemSortType) {
        items.sortWith(sortType.comparator)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View, itemClickListener: (MenuType) -> Unit) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        private var menuItem: MenuItem? = null

        init {
            itemView.setOnClickListener { _ ->
                menuItem?.menuType?.let {
                    itemClickListener(it)
                }
            }
        }

        override val containerView: View? get() = itemView

        fun bind(item: MenuItem?) {
            menuItem = item
            menuItem?.run {
                tv_menu_item_name.text = name
            }
        }
    }
}