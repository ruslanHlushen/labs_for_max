package com.labs.labsapplication.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.labs.labsapplication.R
import com.labs.labsapplication.game.GameActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val menuAdapter: MenuRecyclerAdapter by lazy {
        MenuRecyclerAdapter(createMenuItems(), this::openNextScreen)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        initRecycler()
        initButtonsListeners()
    }

    private fun initRecycler() {
        rv_menu.layoutManager = LinearLayoutManager(this)
        rv_menu.adapter = menuAdapter
    }

    private fun initButtonsListeners() {
        btn_sort_by_id.setOnClickListener { menuAdapter.sortBy(MenuItemSortType.ID()) }
        btn_sort_by_name.setOnClickListener { menuAdapter.sortBy(MenuItemSortType.NAME()) }
    }

    private fun createMenuItems(): List<MenuItem> = (1..4).flatMap { id ->
        MenuType.values().map { type -> MenuItem(id.toLong(), "${getString(type.nameResId)}: $id", type) }
    }

    private fun openNextScreen(menuType: MenuType) = startActivity(GameActivity.createIntent(this, menuType))
}