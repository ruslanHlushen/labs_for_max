package com.labs.labsapplication.game

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF

const val MOVED_DISTANCE = 3f
const val ABS_NON_MOVED_PERIOD_MILLIS = 80L

const val UPDATE_TIME_MILLIS = 60L

data class GameItem(var leftX: Float, var topY: Float, val size: Float = 70.toFloat()) {

    companion object {

        private val oPaint: Paint = Paint().apply {
            setARGB(96, 79, 239, 228)
        }
    }

    val rightX get() = (leftX + size)

    val bottomY get() = (topY + size)

    private val backgroundRadius get() = (size / 14)

    fun draw(canvas: Canvas) =
        canvas.drawRoundRect(RectF(leftX, topY, rightX, bottomY), backgroundRadius, backgroundRadius, oPaint)
}