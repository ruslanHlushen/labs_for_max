package com.labs.labsapplication.game

import android.os.Handler

class CustomViewPresenter {

    private val handler = Handler()
    private val autoMoveAction = Runnable {
        movedItem?.run {
            val itemLeftXCell = ((viewWidth - item.size) / 2)
            val itemTopYCell = ((viewHeight - item.size) / 2)

            val newLeftX = if (itemLeftXCell > item.leftX) {
                item.leftX + MOVED_DISTANCE
            } else {
                item.leftX - MOVED_DISTANCE
            }
            val newTopY = if (itemTopYCell > item.topY) {
                item.topY + MOVED_DISTANCE
            } else {
                item.topY - MOVED_DISTANCE
            }

            var needRedraw = false

            if (Math.abs(itemLeftXCell- newLeftX) > MOVED_DISTANCE) {
                item.leftX = newLeftX
                needRedraw = true
            }
            if (Math.abs(itemTopYCell - newTopY) > MOVED_DISTANCE) {
                item.topY = newTopY
                needRedraw = true
            }

            if (needRedraw) {
                iCustomView?.redrawView()
                startAutoMove()
            }
        }
    }

    private val itemsList = mutableListOf<GameItem>()

    private var movedItem: MovedItem? = null

    val itemsToDraw: List<GameItem> get() = listOfNotNull(*itemsList.toTypedArray(), movedItem?.item)

    private var iCustomView: ICustomView? = null

    private var viewWidth: Int = 0
    private var viewHeight: Int = 0

    fun onAttachView(view: ICustomView) {
        iCustomView = view
    }

    fun onDetachView() {
        stopAutoMove()
        iCustomView = null
    }

    fun setViewSize(width: Int, height: Int) {
        viewWidth = width
        viewHeight = height
    }

    fun setItems(items: List<GameItem>) {
        itemsList.clear()
        itemsList.addAll(items)
    }

    fun onActionDown(xTouch: Float, yTouch: Float) {
        insertNewItemIntoMatrixOrReturnIt()
        removeNearestNewItemForTouch(xTouch, yTouch).run {
            movedItem = MovedItem(copy())
            movedItem?.item?.leftX = xTouch - (0.5f * size)
            movedItem?.item?.topY = yTouch - (0.5f * size)
        }
    }

    fun onActionMove(xTouch: Float, yTouch: Float) {
        stopAutoMove()
        movedItem?.item?.run {
            leftX = xTouch - (0.5f * size)
            topY = yTouch - (0.5f * size)
        }
    }

    fun onActionUp() = insertNewItemIntoMatrixOrReturnIt()

    fun onActionPointerUp() = insertNewItemIntoMatrixOrReturnIt()

    private fun insertNewItemIntoMatrixOrReturnIt() {
        stopAutoMove()
        movedItem?.run {
            if ((System.currentTimeMillis() - startTimeMillis) <= ABS_NON_MOVED_PERIOD_MILLIS) {
                startAutoMove()
            } else {
                itemsList.add(item)
                movedItem = null
            }
        }
    }

    private fun removeNearestNewItemForTouch(xTouch: Float, yTouch: Float): GameItem {
        val index = itemsList.indexOfFirst {
            it.leftX <= xTouch && xTouch <= it.rightX
                    && it.topY <= yTouch && yTouch <= it.bottomY
        }

        return if (index in itemsList.indices) {
            itemsList.removeAt(index)
        } else {
            GameItem(xTouch, yTouch)
        }
    }

    private fun startAutoMove() {
        stopAutoMove()
        handler.postDelayed(autoMoveAction, UPDATE_TIME_MILLIS)
    }

    private fun stopAutoMove() = handler.removeCallbacks(autoMoveAction)
}

private data class MovedItem(
    val item: GameItem,
    val startTimeMillis: Long = System.currentTimeMillis(),
    val originalLeftX: Float = item.leftX,
    val originalTopY: Float = item.topY
)