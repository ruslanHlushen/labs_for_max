package com.labs.labsapplication.game

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.labs.labsapplication.R
import com.labs.labsapplication.main.MenuType
import com.labs.labsapplication.repo.GameRepository
import kotlinx.android.synthetic.main.activity_game.*

/**
 * @author Ruslan Hlushan on 12/10/18.
 */
class GameActivity : AppCompatActivity() {

    companion object {

        private const val KEY_MENU_TYPE = "KEY_MENU_TYPE"

        fun createIntent(context: Context, menuType: MenuType): Intent =
            Intent(context, GameActivity::class.java).apply {
                putExtra(KEY_MENU_TYPE, menuType)
            }
    }

    private val menuType: MenuType by lazy {
        (intent?.extras?.getSerializable(KEY_MENU_TYPE) as? MenuType) ?: MenuType.NEW_GAME
    }

    private var player: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        initViews()
    }

    override fun onResume() {
        super.onResume()
        startPlayer()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
        storeItems()
    }

    private fun releasePlayer() {
        showError({
            player?.stop()
            player?.release()
        }, { player = null })
    }

    private fun startPlayer() {
        releasePlayer()

        player = MediaPlayer().apply {
            val assetFileDescriptor = resources.openRawResourceFd(R.raw.song)
            setDataSource(
                assetFileDescriptor.fileDescriptor,
                assetFileDescriptor.startOffset,
                assetFileDescriptor.declaredLength
            )
            setOnPreparedListener {
                showError({ player?.start() }, { assetFileDescriptor.close() })
            }
            setOnErrorListener { mp, what, extra ->
                Toast.makeText(this@GameActivity, "what = $what, extra = $extra", Toast.LENGTH_LONG).show()
                true
            }
            showError({ prepareAsync() }, {})
        }
    }

    private inline fun showError(action: () -> Unit, finally: () -> Unit) {
        try {
            action()
        } catch (e: Exception) {
            Toast.makeText(this@GameActivity, e.message ?: e.toString(), Toast.LENGTH_LONG).show()
        } finally {
            finally()
        }
    }

    private fun initViews() {
        if (menuType == MenuType.CONTINUE) {
            custom_game_view.itemsToDraw = GameRepository.getSavedItems()
        }
    }

    private fun storeItems() = GameRepository.saveItems(custom_game_view.itemsToDraw)
}