package com.labs.labsapplication.game

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class CustomView
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    View(context, attrs, defStyleAttr), ICustomView {

    private val presenter by lazy { CustomViewPresenter() }

    var itemsToDraw: List<GameItem>
        set(newValue) {
            presenter.setItems(newValue)
            invalidate()
        }
        get() = presenter.itemsToDraw

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        presenter.onAttachView(this)
    }

    override fun onDetachedFromWindow() {
        presenter.onDetachView()
        super.onDetachedFromWindow()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        presenter.setViewSize(measuredWidth, measuredHeight)
    }

    override fun onDraw(canvas: Canvas) =
        itemsToDraw.forEach {
            it.draw(canvas)
        }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        var handled = false

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                handleActionDown(event)
                handled = true
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                handleActionPointerDown(event)
                handled = true
            }
            MotionEvent.ACTION_MOVE -> {
                handleActionMove(event)
                handled = true
            }
            MotionEvent.ACTION_UP -> {
                handleActionUp(event)
                handled = true
            }
            MotionEvent.ACTION_POINTER_UP -> {
                handleActionPointerUp(event)
                handled = true
            }
            MotionEvent.ACTION_CANCEL -> handled = true
            else -> Unit // NOP
        }

        return super.onTouchEvent(event) || handled
    }

    override fun redrawView() = invalidate()

    private fun handleActionDown(event: MotionEvent) {
        val xTouch = event.getX(0)
        val yTouch = event.getY(0)
        presenter.onActionDown(xTouch, yTouch)
        invalidate()
    }

    private fun handleActionPointerDown(event: MotionEvent) {
        val actionIndex = event.actionIndex
        val xTouch = event.getX(actionIndex)
        val yTouch = event.getY(actionIndex)
        presenter.onActionDown(xTouch, yTouch)
        invalidate()
    }

    private fun handleActionMove(event: MotionEvent) {
        val actionIndex = event.actionIndex
        val xTouch = event.getX(actionIndex)
        val yTouch = event.getY(actionIndex)
        presenter.onActionMove(xTouch, yTouch)
        invalidate()
    }

    private fun handleActionUp(event: MotionEvent) {
        presenter.onActionUp()
        invalidate()
    }

    private fun handleActionPointerUp(event: MotionEvent) {
        presenter.onActionPointerUp()
        invalidate()
    }
}