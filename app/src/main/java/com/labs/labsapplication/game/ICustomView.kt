package com.labs.labsapplication.game

/**
 * @author Ruslan Hlushan on 12/11/18.
 */
interface ICustomView {

    fun redrawView()
}