package com.labs.labsapplication.repo

import io.realm.RealmObject

/**
 * @author Ruslan Hlushan on 12/10/18.
 */
open class GameItemDb() : RealmObject() {
    var leftX: Float? = null
    var topY: Float? = null
    var size: Float? = null

    constructor(leftX: Float?, topY: Float?, size: Float?) : this() {
        this.leftX = leftX
        this.topY = topY
        this.size = size
    }
}