package com.labs.labsapplication.repo

import android.content.Context
import com.labs.labsapplication.BuildConfig
import com.labs.labsapplication.game.GameItem
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.kotlin.createObject

/**
 * @author Ruslan Hlushan on 12/10/18.
 */
object GameRepository {

    fun init(appContext: Context) {
        Realm.init(appContext)
        val config = RealmConfiguration.Builder()
            .name("${BuildConfig.DB_NAME}.realm")
            .schemaVersion(BuildConfig.DB_VERSION)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config)
    }

    fun getSavedItems(): List<GameItem> = getRealm { realm ->
        realm.where(GameItemDb::class.java)?.findAll()?.toTypedArray()?.mapNotNull {
            val leftX = it.leftX
            val topY = it.topY
            val size = it.size
            if (leftX != null && topY != null && size != null) {
                GameItem(leftX, topY, size)
            } else {
                null
            }
        } ?: emptyList()
    }

    fun saveItems(list: List<GameItem>) = getRealm { realm ->
        realm.executeTransaction { realmInTransaction ->
            realmInTransaction.where(GameItemDb::class.java)?.findAll()?.deleteAllFromRealm()
            list.forEach {
                val item = realmInTransaction.createObject<GameItemDb>()
                item.leftX = it.leftX
                item.topY = it.topY
                item.size = it.size
            }
        }
    }

    private inline fun <T> getRealm(action: (Realm) -> T): T = Realm.getDefaultInstance().use(action)
}