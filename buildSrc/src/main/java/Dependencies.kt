object ApplicationConfigs {
    const val minSdkVersion = 16
    const val targetSdkVersion = 28
    const val compileSdkVersion = targetSdkVersion
    const val buildToolsVersion = "28.0.3"

    const val DB_VERSION = 1
    const val DB_NAME = "labs"
}

private object Versions {
    const val gradleVersion = "3.2.1"

    const val kotlinVersion = "1.3.11"

    const val supportLibraryVersion = "28.0.0"
    const val constraintLayoutVersion = "1.1.3"

    const val realmVersion = "5.8.1-SNAPSHOT"
}

object Plugins {
    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.gradleVersion}"
    const val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    const val realmPlugin = "io.realm:realm-gradle-plugin:${Versions.realmVersion}"
}

object Deps {
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlinVersion}"

    const val supportAppCompat ="com.android.support:appcompat-v7:${Versions.supportLibraryVersion}"

    const val constraintLayout ="com.android.support.constraint:constraint-layout:${Versions.constraintLayoutVersion}"
    const val recyclerView ="com.android.support:recyclerview-v7:${Versions.supportLibraryVersion}"
    const val cardView ="com.android.support:cardview-v7:${Versions.supportLibraryVersion}"
}